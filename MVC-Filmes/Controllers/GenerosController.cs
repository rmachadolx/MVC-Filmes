﻿using MVC_Filmes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MVC_Filmes.Controllers
{
    public class GenerosController : Controller
    {
        DataClasses1DataContext db = new DataClasses1DataContext();
        // GET: Generos
        public ActionResult Index()
        {
            return View(db.Generos);
        }

        // GET: Generos/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Generos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Generos/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Generos/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Generos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Generos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            return View();

        }

        // POST: Generos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            Generos genero = db.Generos.FirstOrDefault(g => g.IdGenero == id);
            if (genero == null)
            {
                return HttpNotFound();
            }
            db.Generos.DeleteOnSubmit(genero);
            try
            {
                db.SubmitChanges();
            }
            catch (Exception )
            {

                return RedirectToAction("index");
            }
            return View();
        }
    }
}
